var fs = require('fs');
const csv = require('csvtojson')


export const getNoOfMatchesPlayed = (param1) => {
    let obj = {};
    for (let el of param1) {
        obj[el.season] ? obj[el.season]++ : obj[el.season] = 1;
    }
    return obj;
};

export const getNoOfMatchesWonPerTeamPerYear = (param1) => {
    let obj = {};
    for (let el of param1) {
        if (obj[el.winner]) {            
            obj[el.winner][el.season] ? obj[el.winner][el.season]++ : obj[el.winner][el.season] = 1;
            // debugger;
        } else {
            obj[el.winner] = {};
            obj[el.winner][el.season] = 1;
            // debugger;
        }
    }
    return obj;
};

export const getExtraRunsPerTeamForYear = (param1, param2, year) => {
    let arr = [];
    let obj = {};
    for (let el of param1) {
        if (el.season === year) {
            arr.push(el.id);
        }
    }
    for (let el of param2) {
        if (arr.includes(el.match_id)) {
            obj[el.bowling_team] ? obj[el.bowling_team] = parseInt(obj[el.bowling_team]) + parseInt(el.extra_runs) : obj[el.bowling_team] = parseInt(el.extra_runs);
        }
    }
    return obj;
};

export const getEconomicalBowlersForYear = (param1, param2, year) => {
    let arr = [];
    let obj = {};
    let objRuns = {};
    let objOvers = {};
    let temp = '';
    for (let el of param1) {
        if (el.season === year) {
            arr.push(el.id);
        }
    }
    for (let el of param2) {
        if (arr.includes(el.match_id)) {
            objRuns[el.bowler] ? objRuns[el.bowler] =
                parseInt(objRuns[el.bowler]) + parseInt(el.total_runs) - parseInt(el.bye_runs) - parseInt(el.legbye_runs) - parseInt(el.penalty_runs) :
                objRuns[el.bowler] = parseInt(el.total_runs) - parseInt(el.bye_runs) - parseInt(el.legbye_runs) - parseInt(el.penalty_runs);
            if (objOvers[el.bowler]) {
                if (temp !== el.over) {
                    objOvers[el.bowler]++
                    temp = el.over;
                }
            } else {
                objOvers[el.bowler] = 1;
                temp = el.over;
            }
        }
    }
    for (let el in objRuns) {
        obj[el] = objRuns[el] / objOvers[el];
    }
    let sorted = [];
    for (let el in obj) {
        sorted.push([el, obj[el]]);
    }
    sorted.sort((a, b) => a[1] - b[1]);
    let result = {};
    for (let i = 0; i < 10; i++) {
        result[sorted[i][0]] = sorted[i][1];
    }
    return result;
};



/*
csv()
    .fromFile('./src/matches.csv')
    .then((matches) => {
        csv()
            .fromFile('./src/deliveries.csv')
            .then((deliveries) => {

                let obj = {
                    "Number of matches played per year for all the years in IPL" : getNoOfMatchesPlayed(matches),
                    "Number of matches won of per team per year in IPL" : getNoOfMatchesWonPerTeamPerYear(matches),
                    "Extra runs conceded per team in 2016" : getExtraRunsPerTeamForYear(matches, deliveries, '2016'),
                    "Top 10 economical bowlers in 2015" : getEconomicalBowlersForYear(matches, deliveries, '2015')
                }

                fs.writeFileSync("data.json", JSON.stringify(obj), function(err) {
                    if (err) throw err;
                    console.log('complete');
                    }
                );
            });
    })
*/

