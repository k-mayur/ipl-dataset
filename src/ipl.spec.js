import {
  getNoOfMatchesPlayed,
  getNoOfMatchesWonPerTeamPerYear,
  getExtraRunsPerTeamForYear,
  getEconomicalBowlersForYear
} from "./ipl";

describe("IPL module", () => {
  describe("No. of matches played per team for all years, getNoOfMatchesPlayed", () => {
    const matchesSample = [{
        season: 2008
      },
      {
        season: 2009
      },
      {
        season: 2008
      }
    ];
    const expectedResult = {
      2008: 2,
      2009: 1
    };
    test("should exist", () => {
      expect(getNoOfMatchesPlayed).toBeDefined();
    });
    test("should return an object", () => {
      expect(getNoOfMatchesPlayed(matchesSample)).toBeDefined();
      expect(typeof getNoOfMatchesPlayed(matchesSample)).toEqual("object");
      expect(getNoOfMatchesPlayed(matchesSample)).toEqual(expectedResult);
    });
  });
  describe("No. of matches won per team per year, getNoOfMatchesWonPerTeamPerYear", () => {

    const matchesSample = [{
        winner: 'Team1',
        season: 2008
      },
      {
        winner: 'Team2',
        season: 2009
      },
      {
        winner: 'Team1',
        season: 2008
      },
      {
        winner: 'Team1',
        season: 2008
      },
      {
        winner: 'Team2',
        season: 2009
      },
      {
        winner: 'Team2',
        season: 2008
      }
    ];
    const expectedResult = {
      Team1: {
        2008: 3
      },
      Team2: {
        2008: 1,
        2009: 2
      }
    };
    test("should exist", () => {
      expect(getNoOfMatchesWonPerTeamPerYear).toBeDefined();
    });
    test("should return an object", () => {
      expect(getNoOfMatchesWonPerTeamPerYear(matchesSample)).toBeDefined();
      expect(typeof getNoOfMatchesWonPerTeamPerYear(matchesSample)).toEqual("object");
      expect(getNoOfMatchesWonPerTeamPerYear(matchesSample)).toEqual(expectedResult);
    });
  });
  describe("Extra runs conceeded per team for year, getExtraRunsPerTeamForYear", () => {
    const matchesSample = [
      {
        season : '2015',
        id : 1
      },
      {
        season : '2016',
        id : 2
      },
      {
        season : '2016',
        id : 3
      },
    ];
    const deliverySample = [
      {
        match_id : 1,
        bowling_team : 'Team1',
        extra_runs : 4
      },
      {
        match_id : 2,
        bowling_team : 'Team2',
        extra_runs : 8
      },
      {
        match_id : 3,
        bowling_team : 'Team3',
        extra_runs : 1
      },
      {
        match_id : 3,
        bowling_team : 'Team2',
        extra_runs : 2
      },
    ];
    const expectedResult = {
      'Team2' : 10,
      'Team3' : 1
    };
    test("should exist", () => {
      expect(getExtraRunsPerTeamForYear).toBeDefined();
    });
    test("should return an object", () => {
      expect(getExtraRunsPerTeamForYear(matchesSample, deliverySample, '2016')).toBeDefined();
      expect(typeof getExtraRunsPerTeamForYear(matchesSample, deliverySample, '2016')).toEqual("object");
      expect(getExtraRunsPerTeamForYear(matchesSample, deliverySample, '2016')).toEqual(expectedResult);
    });
  });
  describe("Economical bowlers for year, getEconomicalBowlersForYear", () => {
    const matchesSample = [
      {
        season : '2015',
        id : 1
      },
      {
        season : '2015',
        id : 2
      },
      {
        season : '2016',
        id : 3
      },
    ];
    const deliverySample = [
      {
        match_id : 3,
        bowler : 'bowler1',
        over : '1',
        bye_runs : '0',
        legbye_runs : '0',
        penalty_runs : '0',
        total_runs : '5'
      },
      {
        match_id : 1,
        bowler : 'bowler1',
        over : '1',
        bye_runs : '0',
        legbye_runs : '0',
        penalty_runs : '0',
        total_runs : '8'
      },
      {
        match_id : 2,
        bowler : 'bowler2',
        over : '4',
        bye_runs : '0',
        legbye_runs : '0',
        penalty_runs : '0',
        total_runs : '5'
      },
      {
        match_id : 1,
        bowler : 'bowler1',
        over : '5',
        bye_runs : '0',
        legbye_runs : '0',
        penalty_runs : '0',
        total_runs : '10'
      },
    ];
    const expectedResult = {
      'bowler1' : 9,
      'bowler2' : 5
    };
    test("should exist", () => {
      expect(getEconomicalBowlersForYear).toBeDefined();
    });
    test("should return an object", () => {
      expect(getEconomicalBowlersForYear(matchesSample, deliverySample, '2015')).toBeDefined();
      expect(typeof getEconomicalBowlersForYear(matchesSample, deliverySample, '2015')).toEqual("object");
      //change top ten to top 2 for sample test
      //line 80 ipl.js replace 10 in for loop by 2 before running npm test i.e. "for (let i = 0; i < 2; i++)"
      expect(getEconomicalBowlersForYear(matchesSample, deliverySample, '2015')).toEqual(expectedResult);
    });
  });
});